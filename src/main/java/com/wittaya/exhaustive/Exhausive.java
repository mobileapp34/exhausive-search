/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.exhaustive;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @author AdMiN
 */
public class Exhausive {
     final Map<String, Map<String, Integer>> distances;

    public Exhausive(Map<String, Map<String, Integer>> distances) {
        this.distances = distances;
    }

    public static <T> void swap(T[] array, int first, int second) {
        T temp = array[first];
        array[first] = array[second];
        array[second] = temp;
    }
 static <T> void Permutations(T[] permutation, List<T[]> permutations, int n) {
        if (n <= 0) {
            permutations.add(permutation);
            return;
        }
        T[] tempPermutation = Arrays.copyOf(permutation, permutation.length);
        for (int i = 0; i < n; i++) {
            swap(tempPermutation, i, n - 1);

            Permutations(tempPermutation, permutations, n - 1);
            swap(tempPermutation, i, n - 1);
        }
    }
 
 static <T> List<T[]> allpermutations(T[] original) {
        List<T[]> permutations = new ArrayList<>();
        Permutations(original, permutations, original.length);
        return permutations;
    }

    public int pathDistance(String[] path) {
        String last = path[0];
        int distance = 0;
        for (String next : Arrays.copyOfRange(path, 1, path.length)) {
            distance += distances.get(last).get(next);

            last = next;
        }
        return distance;
    }
public String[] findShortestPath() {
        String[] cities = distances.keySet().toArray(String[]::new);
        List<String[]> paths = permutations(cities);
        String[] shortestPath = null;
        int minDistance = Integer.MAX_VALUE;
        for (String[] path : paths) {
            int distance = pathDistance(path);

            distance += distances.get(path[path.length - 1]).get(path[0]);
            if (distance < minDistance) {
                minDistance = distance;
                shortestPath = path;
            }
        }

        shortestPath = Arrays.copyOf(shortestPath, shortestPath.length + 1);
        shortestPath[shortestPath.length - 1] = shortestPath[0];
        return shortestPath;
    }
    public static void main(String[] args) {
        Map<String, Map<String, Integer>> vtDistances = Map.of(
                "Osaka", Map.of(
                        "WritJohnson", 67,
                        "RiverWood", 46,
                        "BrightWood", 55,
                        "Casidy", 75),
                "WritJohnson", Map.of(
                        "Osaka", 67,
                        "RiverWood", 91,
                        "BrightWood", 122,
                        "Casidy", 153),
                "RiverWood", Map.of(
                        "Osaka", 46,
                        "WritJohnson", 91,
                        "BrightWood", 98,
                        "Casidy", 65),
                "BrightWood", Map.of(
                        "Osaka", 55,
                        "WritJohnson", 122,
                        "RiverWood", 98,
                        "Casidy", 40),
                "Casidy", Map.of(
                        "Osaka", 75,
                        "WritJohnson", 153,
                        "RiverWood", 65,
                        "BrightWood", 40));
        Exhausive test = new Exhausive(vtDistances);
        String[] shortestPath = test.findShortestPath();
        int distance = test.pathDistance(shortestPath);
        System.out.println("The shortest path is " + Arrays.toString(shortestPath) + " in " +
                distance + " miles.");
    }


}
